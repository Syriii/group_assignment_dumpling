/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Abstract.User;
import Business.Users.Customer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author harshalneelkamal
 */
public class CustomerDirectory {
    
    private List<Customer> customerList;
    
    public CustomerDirectory(){
        customerList = new ArrayList<>();
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> supplierList) {
        this.customerList = supplierList;
    }
    
    public Customer addCustomer(Date dateCreated, String password, String userName){
        Customer c = new Customer(dateCreated, password, userName);
        customerList.add(c);
        return c;
    }
    
    public void removeCustomer(Customer c){
        customerList.remove(c);
    }
    
    public Customer searchCustomer(String customerNmae){
        for(Customer c: customerList){
            if(c.getUserName().equals(customerNmae) )
                return c;
        }
        return null;
    }
    
}
